var path = require('path');
var webpack = require('webpack');
var LiveReloadPlugin = require('webpack-livereload-plugin');
var WebpackNotifierPlugin = require('webpack-notifier');
var extend = require('extend');

var webpackConfig = {
	entry: {
		app: './app/frontend/js/index'
	},
	output: {
		path: path.resolve(__dirname, '../public/built'),
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				exclude: /(node_modules|bower_components)/,
				query: {
					presets: ["es2015", "stage-0", "react"]
				}
			},
			{ test: /\.css$/, loader: 'style!css', exclude: /node_modules/ },
			{ test: /\.less$/, loader: 'style!css!less' },
			{ test: /\.styl$/, loader: 'style!css!stylus' },
			{ test: /\.coffee$/, loader: 'coffee-loader', exclude: /node_modules/ },
			{ test: /\.cjsx$/, loaders: ['coffee', 'cjsx'], exclude: /node_modules/ },
			{ test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file" },
			{ test: /\.(woff|woff2)$/, loader:"url?prefix=font/&limit=25000" },
			{ test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream" },
			{ test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml" }
		],
	},
	resolve: {
		extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx", ".coffee", ".cjsx", ".styl", ".css", ".less"]
	}
};

var environmentExtensions = {};
if (process.env.NODE_ENV === 'production') {
	environmentExtensions = {
		devtool: 'source-map',
		plugins: [
			// I got this from: http://moduscreate.com/optimizing-react-es6-webpack-production-build/
			new webpack.DefinePlugin({
				'process.env': {
					'NODE_ENV': JSON.stringify('production')
				}
			}),
			new webpack.optimize.UglifyJsPlugin()
		]
	}
} else {
	environmentExtensions = {
		debug: true,
		devtool: 'eval',
		plugins: [
			new webpack.HotModuleReplacementPlugin(),
			new webpack.NoErrorsPlugin(),
			new LiveReloadPlugin({port: 35730}),
			new WebpackNotifierPlugin({alwaysNotify: true}),
			// Say when a build took place. Handy.
			// From: https://github.com/webpack/webpack/issues/1499#issuecomment-155064216
			function() {
				this.plugin('watch-run', function(watching, callback) {
					console.log('Begin compile at ' + new Date());
					callback();
				})
			}
		],
		// docs: https://webpack.github.io/docs/node.js-api.html#compiler
		watchOptions: {
			aggregateTimeout: 300
		}
	}
}

webpackConfig = extend(true, {}, webpackConfig, environmentExtensions);
module.exports = webpackConfig;