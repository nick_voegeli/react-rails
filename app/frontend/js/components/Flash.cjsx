React = require 'react'
{ Component, PropTypes } = React
spinCss = require '../../styles/spin.css'
ReduxToastr = require('react-redux-toastr').default
{ toastr } = require 'react-redux-toastr'

Flash = React.createClass
	displayAs: 'Flash'
	propTypes:
		flash: PropTypes.shape
			title: PropTypes.string
			type: PropTypes.string
			message: PropTypes.string
		clearFlash: PropTypes.func.isRequired

	componentDidUpdate: ->
		{ flash } = @props
		if flash && flash.type && flash.message
			@showMessage flash.type, flash.title, flash.message

	showMessage: (type, title, message) ->
		toastr[type](title, message)
		@props.clearFlash()

	render: ->
		{ flash } = @props
		<div id="flash" className={ flash.type }>
			<ReduxToastr timeOut={4000} newestOnTop={false} position="top-left"/>
		</div>

module.exports = Flash
