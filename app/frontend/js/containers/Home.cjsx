React = require 'react'
{ Component } = React

class Home extends Component
	displayName: 'Home'
	
	render: ->
		<div>
			This is the Home container.
		</div>

module.exports = Home
