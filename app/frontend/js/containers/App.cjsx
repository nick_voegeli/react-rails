{ connect } = require 'react-redux'
Bootstrap = require 'react-bootstrap'
Flash = require '../components/Flash'
React = require 'react'
{ Component, PropTypes } = React
require 'style!css!bootstrap/dist/css/bootstrap.min.css'
require 'react-redux-toastr/src/less/index'
{ clearFlash } = require '../actions/flash'

App = React.createClass
	displayName: 'App'
	propTypes:
		flash: PropTypes.object,
		clearFlash: PropTypes.func.isRequired

	contextTypes:
		router: PropTypes.object

	# componentDidMount: ->
	# 	@context.router.registerTransitionHook(@transitionHook.bind(this))

	# transitionHook: (nextLocation) ->
	#		This function can be used to check authorization expiration

	render: ->
		{ children, clearFlash, flash } = @props
		<div className="app">
			<div className="container-fluid app-container">
				{ children }
			</div>
			<Flash flash={ flash } clearFlash={ clearFlash }/>
		</div>

mapStateToProps = (state) ->
	flash: state.flash

module.exports = connect(mapStateToProps, { clearFlash })(App)
