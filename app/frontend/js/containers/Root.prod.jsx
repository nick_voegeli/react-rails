import React, { Component, PropTypes } from 'react'
import { Provider } from 'react-redux'
import { ReduxRouter } from 'redux-simple-router'
import { Router } from 'react-router'

export default class Root extends Component {
	static propTypes = {
		history: PropTypes.object.isRequired,
		routes: PropTypes.func.isRequired,
		store: PropTypes.object.isRequired
	};

	render() {
		const { store } = this.props
		return (
			<Provider store={store}>
				<div className="root-container">
					<Router history={this.props.history}>
						{this.props.routes(store)}
					</Router>
				</div>
			</Provider>
		)
	}
}

Root.propTypes = {
	store: PropTypes.object.isRequired
}
