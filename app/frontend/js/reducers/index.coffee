{ combineReducers } = require 'redux'
{ flash } = require './flash'
toastrReducer = require('react-redux-toastr').reducer
formReducer = require('redux-form').reducer
Immutable = require('seamless-immutable')
formNormalizers = require '../lib/formNormalizers'

{ routeReducer } = require 'redux-simple-router'

rootReducer = combineReducers({
	form: formReducer.normalize formNormalizers
	flash: flash
	toastr: toastrReducer
	routing: routeReducer
})

module.exports = rootReducer

