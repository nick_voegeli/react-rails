{ combineReducers } = require 'redux'
{ CLEAR_FLASH, DO_FLASH } = require '../actions/flash'
Immutable = require('seamless-immutable')

flash = (state = Immutable({}), action) ->
	switch action.type
		when DO_FLASH
			state = Immutable
				title: action.payload.title
				type: action.payload.type
				message: action.payload.message
		when CLEAR_FLASH
			state = Immutable({})
	state

module.exports = {
	flash
}
