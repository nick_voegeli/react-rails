forceLowerCase = (value) -> value and value.toLowerCase()

username = forceLowerCase
email = forceLowerCase

module.exports =
	login:
		username: username
		email: email
	student:
		username: username
		email: email
	researcher:
		username: username
		email: email
