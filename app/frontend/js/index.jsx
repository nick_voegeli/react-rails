import App from './containers/App'
// For the `babel-polyfill` dependency below, see
// http://rackt.org/redux/docs/advanced/AsyncActions.html
// and search `note on fetch`
import 'babel-polyfill'
import configureStore from './store/configureStore'
import React from 'react'
import { render } from 'react-dom'
import Root from './containers/Root'
import { Provider } from 'react-redux'
import routes from './routes'
import { createHistory } from 'history'
import { useRouterHistory } from 'react-router'

const historyConfig = { basename: '' }
const history = useRouterHistory(createHistory)(historyConfig)

const initialState = {}
const store = configureStore({ initialState, history })

render(
	<Root history={history} routes={routes} store={store} />,
	document.getElementById('root')
)
