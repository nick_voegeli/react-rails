import { createStore, applyMiddleware, compose } from 'redux'
import { syncHistory } from 'redux-simple-router'
import DevTools from '../containers/DevTools'
import reducer from '../reducers/index.coffee'
import thunk from 'redux-thunk'

// Content initially adapted from the "Real World" redux example:
// https://github.com/rackt/redux/tree/master/examples/real-world
// Updated to work with router.
// https://github.com/davezuko/react-redux-starter-kit/blob/ee582516737240602ac2d6d35ee3a2dda88c421b/src/redux/configureStore.js

export default function configureStore({initialState = {}, history}) {
	const routerMiddleware = syncHistory(history)

	let middleware = applyMiddleware(thunk, routerMiddleware)
	middleware = compose(middleware, DevTools.instrument())

	const store = middleware(createStore)(reducer, initialState)

	if (module.hot) {
		// Enable Webpack hot module replacement for reducers
		module.hot.accept('../reducers/index.coffee', () => {
			const nextRootReducer = require('../reducers/index.coffee')
			store.replaceReducer(nextRootReducer)
		})
	}

	return store
}
