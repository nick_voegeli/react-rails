import { createStore, applyMiddleware, compose } from 'redux'
import { syncHistory, routeReducer } from 'redux-simple-router'
import reducer from '../reducers/index.coffee'
import thunk from 'redux-thunk'

export default function configureStore({initialState = {}, history}) {
	const routerMiddleware = syncHistory(history)

	let middleware = applyMiddleware(thunk, routerMiddleware)
	middleware = compose(middleware)

	const store = middleware(createStore)(reducer, initialState)

	return store
}
