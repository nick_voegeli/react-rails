// Content adapted from the "Real World" redux example:
// https://github.com/rackt/redux/tree/master/examples/real-world

// In development, we use hot module loading in webpack and the react-devtools
if (process.env.NODE_ENV === 'production') {
  module.exports = require('./configureStore.prod')
} else {
  module.exports = require('./configureStore.dev')
}
