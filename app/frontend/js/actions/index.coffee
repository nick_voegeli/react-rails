fetch = require 'isomorphic-fetch'
extend = require 'extend'

# This is from https://github.com/github/fetch
# We don't have to use it, but it seemed like a good way to have control, and
# I also liked using .catch() instead of passing a second argument to .then
# for error cases
checkStatus = (response) ->
	if response.status >= 200 and response.status < 300
		response
	else
		error = new Error(response.statusText)
		error.response = response
		throw error

parseJson = (response) ->
	response.json()

# If you're making an API call to anywhere but our server, you should
# set 'includeToken' to false in the options.
fetchJson = (url, inputOptions = {}) ->
	defaultOptions =
		method: 'get'
		body: null
		includeToken: true
	options = extend {}, defaultOptions, inputOptions

	fetchOptions =
		method: options.method
		headers:
			'Accept': 'application/json'
			'Content-Type': 'application/json'

	fetchOptions.body = JSON.stringify options.body if options.body
	fetch url, fetchOptions
	.then checkStatus
	.then parseJson

module.exports = {
	fetchJson
}
