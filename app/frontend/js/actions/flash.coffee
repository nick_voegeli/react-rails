DO_FLASH = 'DO_FLASH'
CLEAR_FLASH = 'CLEAR_FLASH'

module.exports =
	DO_FLASH: DO_FLASH
	CLEAR_FLASH: CLEAR_FLASH

	doFlash: (flashType, title, message) ->
		type: DO_FLASH
		payload:
			title: title
			type: flashType
			message: message

	clearFlash: ->
		type: CLEAR_FLASH
