class ApplicationController < ActionController::Base
  # Don't use CSRF for JSON requests.
  # See http://api.rubyonrails.org/classes/ActionController/RequestForgeryProtection.html
  protect_from_forgery
  skip_before_action :verify_authenticity_token, if: :json_request?

  protected
  def json_request?
    request.format.json?
  end
end
