# **react-rails**

Basic implementation of React on Rails using [redux](https://github.com/reactjs/redux), with some other goodies rolled in, including:

* [webpack](https://webpack.github.io/) with the [webpack-livereload-plugin](https://www.npmjs.com/package/webpack-livereload-plugin)
* [redux-devtools](https://github.com/gaearon/redux-devtools) for debugging in dev
* Many webpack loaders: coffeescript, cjsx, less, stylus, es6, etc.
* [react-bootstrap](https://react-bootstrap.github.io/)
* [redux-thunk](https://github.com/gaearon/redux-thunk)
* [redux-form](https://github.com/erikras/redux-form)
* [redux-simple-router](https://github.com/reactjs/react-router-redux)
* [react-redux-toastr](https://www.npmjs.com/package/react-redux-toastr) with a custom-built flash component

## Project Structure

Rails can be used as normal; nothing has moved or changed.

React files are located in `app/frontend/`.  These files are compiled using webpack by running `npm run build`.  Webpack can be set to watch using `npm run buildw`.  Bundle files compiles into `public/built/`.

## Installation

1. Clone the repo
1. `bundle install`
1. `npm install`
1. `npm run build`
1. `rails s` and you're all set!